#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>

void print_bytes(BYTE *data, UINT32 len){
    for(UINT32 i = 0; i < len; ++i){
        printf("%02x", data[i]);
    }
}

void sprint_bytes(char *str, BYTE *data, UINT32 len){
    for(UINT32 i = 0; i < len; ++i){
        sprintf(str + (i*2), "%02x", data[i]);
    }
}

// requires buffer of 36+1 bytes
void encode_uuid(char *str, TSS_UUID uuid){
    sprintf(str, "%08x-%04x-%04x-%02x%02x-",
           uuid.ulTimeLow,
           uuid.usTimeMid,
           uuid.usTimeHigh,
           uuid.bClockSeqHigh,
           uuid.bClockSeqLow
    );
    sprint_bytes(str + 24, uuid.rgbNode, 6);
}

TSS_UUID decode_uuid(const char *str){
    TSS_UUID uuid;

    int n = sscanf(str, "%8x-%4x-%4x-%2x%2x-%2x%2x%2x%2x%2x%2x",
           &uuid.ulTimeLow,
           &uuid.usTimeMid,
           &uuid.usTimeHigh,
           &uuid.bClockSeqHigh,
           &uuid.bClockSeqLow,
           &uuid.rgbNode[0],
           &uuid.rgbNode[1],
           &uuid.rgbNode[2],
           &uuid.rgbNode[3],
           &uuid.rgbNode[4],
           &uuid.rgbNode[5]);
    if(n != 11){
        printf("decode uuid error\n");
        throw __LINE__;
    }

    return uuid;
}

void print_uuid(TSS_UUID *uuid){
    char s[37];
    encode_uuid(s, *uuid);
    printf("%s", s);
}

void print_error(TSS_RESULT r){
    printf("tspi error: %s\n", Trspi_Error_String(r));
}

void check_tss(TSS_RESULT r, int line){
    if(r != TSS_SUCCESS){
        print_error(r);
        throw line;
    }
}

TSS_HCONTEXT tss_open_context_local(){
    TSS_RESULT r;

    TSS_HCONTEXT ctx;
    r = Tspi_Context_Create(&ctx);
    check_tss(r, __LINE__);
    r = Tspi_Context_Connect(ctx, NULL);
    check_tss(r, __LINE__);

    return ctx;
}

void tss_close_context(TSS_HCONTEXT ctx){
    Tspi_Context_FreeMemory(ctx, NULL);
    Tspi_Context_Close(ctx);
}

TSS_HTPM tss_get_tpm(TSS_HCONTEXT ctx){
    TSS_RESULT r;

    // get TPM
    TSS_HTPM tpm;
    r = Tspi_Context_GetTpmObject(ctx, &tpm);
    check_tss(r, __LINE__);

    return tpm;
}

void tss_tpm_reset_dalock(TSS_HTPM tpm){
    TSS_RESULT r;

    // reset TPM dictionary attack lock
    r = Tspi_TPM_SetStatus(tpm, TSS_TPMSTATUS_RESETLOCK, TRUE);
    check_tss(r, __LINE__);
}

void tss_auth_tpm(TSS_HTPM tpm, char *owner_key, UINT32 keylen){
    TSS_RESULT r;

    // get TPM policy
    TSS_HPOLICY policy;
    r = Tspi_GetPolicyObject(tpm, TSS_POLICY_USAGE, &policy);
    check_tss(r, __LINE__);
    // authenticate TPM
    r = Tspi_Policy_SetSecret(policy, TSS_SECRET_MODE_PLAIN, keylen, (BYTE *)owner_key);
    check_tss(r, __LINE__);
}

void tss_auth_srk(TSS_HCONTEXT ctx, char *srk_key, UINT32 keylen){
    TSS_RESULT r;

    TSS_HKEY srk;
    r = Tspi_Context_LoadKeyByUUID(ctx, TSS_PS_TYPE_SYSTEM, TSS_UUID_SRK, &srk);
    check_tss(r, __LINE__);
    // get SRK policy
    TSS_HPOLICY srkpolicy;
    r = Tspi_GetPolicyObject(srk, TSS_POLICY_USAGE, &srkpolicy);
    check_tss(r, __LINE__);
    // authenticate SRK
    r = Tspi_Policy_SetSecret(srkpolicy, TSS_SECRET_MODE_PLAIN, keylen, (BYTE *)srk_key);
    check_tss(r, __LINE__);
}

TSS_HKEY tss_load_key(TSS_HCONTEXT ctx, TSS_UUID uuid){
    TSS_RESULT r;

    // load key
    TSS_HKEY key;
    r = Tspi_Context_LoadKeyByUUID(ctx, TSS_PS_TYPE_SYSTEM, uuid, &key);
    check_tss(r, __LINE__);

    return key;
}

void tss_get_rsa_pub(TSS_HKEY key, RSA *sslkey){
    TSS_RESULT r;
    UINT32 keylen;
    BYTE *keypub;

    r = Tspi_GetAttribData(key, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_MODULUS, &keylen, &keypub);
    check_tss(r, __LINE__);
    BIGNUM *n = BN_bin2bn(keypub, keylen, NULL);

    r = Tspi_GetAttribData(key, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_EXPONENT, &keylen, &keypub);
    check_tss(r, __LINE__);
    BIGNUM *e = BN_bin2bn(keypub, keylen, NULL);

    RSA_set0_key(sslkey, n, e, NULL);
}

int tss_quote(TSS_HCONTEXT ctx, TSS_HTPM tpm, TSS_UUID uuid, UINT32 pcr_mask, BYTE *nonce,
              BYTE *data, UINT32 *datalen, BYTE *sig, UINT32 *siglen){

    TSS_RESULT r;

    // load identity key
    TSS_HKEY key = tss_load_key(ctx, uuid);

    // create PCRS object
    TSS_HPCRS hpcrs;
    r = Tspi_Context_CreateObject(ctx, TSS_OBJECT_TYPE_PCRS, TSS_PCRS_STRUCT_INFO, &hpcrs);
    check_tss(r, __LINE__);
    // select PCR indexes
    for(unsigned i = 0; i < PCR_COUNT; ++i){
        if(pcr_mask & (1 << i)){
            r = Tspi_PcrComposite_SelectPcrIndex(hpcrs, i);
            check_tss(r, __LINE__);
        }
    }

    // set nonce
    TSS_VALIDATION validation;
    validation.ulExternalDataLength = NONCE_LEN;
    validation.rgbExternalData = nonce;

    // get quote
    r = Tspi_TPM_Quote(tpm, key, hpcrs, &validation);
    check_tss(r, __LINE__);

    memcpy(data, validation.rgbData, validation.ulDataLength);
    *datalen = validation.ulDataLength;
    memcpy(sig, validation.rgbValidationData, validation.ulValidationDataLength);
    *siglen = validation.ulValidationDataLength;

    // extract composite hash
    BYTE chash[PCR_LEN];
    memcpy(chash, validation.rgbData + 8, PCR_LEN);
    printf("composite hash: ");
    print_bytes(chash, PCR_LEN);
    printf("\n");

    return 0;
}
