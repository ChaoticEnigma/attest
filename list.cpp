
#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>

int main(int argc, char **argv){

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    int eline = 0;
    TSS_RESULT r;
    TSS_HCONTEXT ctx = tss_open_context_local();

    try {
        // get TPM
        TSS_HTPM tpm = tss_get_tpm(ctx);
        // auth TPM
        tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));
        // auth SRK
        tss_auth_srk(ctx, srk_secret, strlen(srk_secret));

        // get registered keys
        UINT32 keylistsize;
        TSS_KM_KEYINFO2 *keyinfolist;
        r = Tspi_Context_GetRegisteredKeysByUUID2(ctx, TSS_PS_TYPE_SYSTEM, NULL, &keylistsize, &keyinfolist);
        check_tss(r, __LINE__);

        const TSS_UUID srkuuid = TSS_UUID_SRK;

        // get key info
        for(UINT32 i = 0; i < keylistsize; ++i){
            TSS_KM_KEYINFO2 *keyinfo = keyinfolist + i;

            // load key
            tss_tpm_reset_dalock(tpm);
            TSS_HKEY key = tss_load_key(ctx, keyinfo->keyUUID);

            printf("key %u:\n  ", i);
            print_uuid(&keyinfo->keyUUID);
            if(memcmp(&keyinfo->keyUUID, &srkuuid, sizeof(TSS_UUID)) == 0)
                printf(" (SRK)");
            printf("\n  parent: ");
            print_uuid(&keyinfo->parentKeyUUID);
            if(memcmp(&keyinfo->parentKeyUUID, &srkuuid, sizeof(TSS_UUID)) == 0)
                printf(" (SRK)");
            printf("\n");

            // key flags
            UINT32 keyflags;
            r = Tspi_GetAttribUint32(key, TSS_TSPATTRIB_KEY_INFO, TSS_TSPATTRIB_KEYINFO_KEYFLAGS, &keyflags);
            check_tss(r, __LINE__);
            printf("  keyflags: %08x (%s %s %s)\n", keyflags,
                   keyflags & 1 ? "REDIRECTION" : "!REDIRECTION",
                   keyflags & 2 ? "MIGRATABLE" : "!MIGRATABLE",
                   keyflags & 4 ? "VOLATILE" : "!VOLATILE");

//            printf("  pstype: %u\n", keyinfo->persistentStorageType);
//            printf("  ppstype: %u\n", keyinfo->persistentStorageTypeParent);
            printf("  loaded: %u\n", keyinfo->fIsLoaded);
            printf("  auth: %u\n", keyinfo->bAuthDataUsage);
            printf("  data: %u bytes\n", keyinfo->ulVendorDataLength);
            if(keyinfo->ulVendorDataLength){
                printf("    ");
                print_bytes(keyinfo->rgbVendorData, keyinfo->ulVendorDataLength);
                printf("\n");
            }

            UINT32 keysize;
            r = Tspi_GetAttribUint32(key, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_KEYSIZE, &keysize);
            check_tss(r, __LINE__);
            printf("  keylen: %u\n", keysize);

            UINT32 nprimes;
            r = Tspi_GetAttribUint32(key, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_PRIMES, &nprimes);
            check_tss(r, __LINE__);
            printf("  primes: %u\n", nprimes);

            // SRK needs to be loaded first to get public data
            if(memcmp(&keyinfo->keyUUID, &srkuuid, sizeof(TSS_UUID)) == 0){
                UINT32 keylen;
                BYTE *keypub;
                // get SRK public key
                r = Tspi_TPM_OwnerGetSRKPubKey(tpm, &keylen, &keypub);
                check_tss(r, __LINE__);

//                printf("  publen: %u bytes\n  ", keylen);
//                print_bytes(keypub, keylen);
//                printf("\n");
            }

            // get public key
            RSA *sslkey = RSA_new();
            tss_get_rsa_pub(key, sslkey);

            char uustr[37];
            encode_uuid(uustr, keyinfo->keyUUID);
            char fname[100];
            sprintf(fname, "pubkey_%s.pem", uustr);

            // write PEM public key
            FILE *fout = fopen(fname, "wx");
            if(fout != NULL){
                PEM_write_RSA_PUBKEY(fout, sslkey);
                fclose(fout);
                printf("  wrote %s\n", fname);
            } else {
                printf("  failed to write %s\n", fname);
            }

            printf("\n");
        }

    } catch(int l){
        eline = l;
    }

    tss_close_context(ctx);
    return eline;
}
