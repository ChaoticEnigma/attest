
import os, sys
import binascii
import rsa
import secrets
import socket

PORT = 17771

if len(sys.argv) != 3:
    print("Usage: challenge.py <server IP> <PEM TPM public AIK>")
    exit(1)

server = sys.argv[1]
fpubkey = sys.argv[2]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((server, PORT))

nonce = secrets.token_hex(20)
print("nonce: {}".format(nonce))

s.send((nonce+"\n").encode())

data = s.recv(1024)
print("quote: " + data.decode())

s.close()

quote = data.decode().split(':')

if len(quote) != 2:
    exit(2)

msg_bin = binascii.unhexlify(quote[0])
sig_bin = binascii.unhexlify(quote[1])

pub_pem = open(fpubkey, 'rb').read()

pubkey = rsa.PublicKey.load_pkcs1_openssl_pem(pub_pem)

try:
    rsa.verify(msg_bin, sig_bin, pubkey)
    print("Signature OK")
    
    pcr_bin = msg_bin[8:28]
    print("PCR Composite Hash: " + binascii.hexlify(pcr_bin).decode())

except rsa.pkcs1.VerificationError:
    print("Signature failed")
