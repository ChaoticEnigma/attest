
#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>

int main(int argc, char **argv){

    if(argc != 2){
        printf("Usage: create <uuid>\n");
        return -1;
    }

    TSS_UUID uuid = decode_uuid(argv[1]);

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    printf("Create Key\nUUID: ");
    print_uuid(&uuid);
    printf("\n");

    int eline = 0;
    TSS_RESULT r;
    TSS_HCONTEXT ctx = tss_open_context_local();

    try {
        // get TPM
        TSS_HTPM tpm = tss_get_tpm(ctx);
        // auth TPM
        tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));

        TSS_HKEY srk;
        r = Tspi_Context_LoadKeyByUUID(ctx, TSS_PS_TYPE_SYSTEM, TSS_UUID_SRK, &srk);
        check_tss(r, __LINE__);
        // auth SRK
        tss_auth_srk(ctx, srk_secret, strlen(srk_secret));

        TSS_HKEY key;
        r = Tspi_Context_CreateObject(ctx, TSS_OBJECT_TYPE_RSAKEY, TSS_KEY_TYPE_SIGNING | TSS_KEY_SIZE_2048, &key);
        check_tss(r, __LINE__);

        // create new key
        r = Tspi_Key_CreateKey(key, srk, 0);
        check_tss(r, __LINE__);

        // register key
        r = Tspi_Context_RegisterKey(ctx, key, TSS_PS_TYPE_SYSTEM, uuid, TSS_PS_TYPE_SYSTEM, TSS_UUID_SRK);
        check_tss(r, __LINE__);

        // get public key
        RSA *sslkey = RSA_new();
        tss_get_rsa_pub(key, sslkey);

        char uustr[37];
        encode_uuid(uustr, uuid);
        char fname[100];
        sprintf(fname, "pubkey_%s.pem", uustr);

        // write PEM public key
        FILE *fout = fopen(fname, "wx");
        if(fout != NULL){
            PEM_write_RSA_PUBKEY(fout, sslkey);
            fclose(fout);
            printf("  wrote %s\n", fname);
        } else {
            printf("  failed to write %s\n", fname);
        }

    } catch(int l){
        eline = l;
    }

    tss_close_context(ctx);
    return eline;
}
