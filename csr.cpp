
#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>

void check_openssl(int r, int line){
    if(r != 1){
        printf("openssl error\n");
        throw line;
    }
}

int main(int argc, char **argv){

    if(argc != 2){
        printf("Usage: csr <uuid>\n");
        return -1;
    }

    TSS_UUID uuid = decode_uuid(argv[1]);

    printf("Generate CSR for Key:\n");
    print_uuid(&uuid);
    printf("\n");

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    int eline = 0;
    TSS_RESULT r;
    TSS_HCONTEXT ctx = tss_open_context_local();

    try {
        // get TPM
        TSS_HTPM tpm = tss_get_tpm(ctx);
        // auth TPM
        tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));
        // auth SRK
        tss_auth_srk(ctx, srk_secret, strlen(srk_secret));
        tss_tpm_reset_dalock(tpm);

        // load identity key
        TSS_HKEY key;
        r = Tspi_Context_LoadKeyByUUID(ctx, TSS_PS_TYPE_SYSTEM, uuid, &key);
        check_tss(r, __LINE__);

        TSS_HHASH hash;
        r = Tspi_Context_CreateObject(ctx, TSS_OBJECT_TYPE_HASH, TSS_HASH_SHA1, &hash);
        check_tss(r, __LINE__);

        const char *szCountry       = "US";
        const char *szProvince      = "New-Hampshire";
        const char *szCity          = "Durham";
        const char *szOrganization  = "UNH";
        const char *szOrgUnit       = "ECE796-Final";
        const char *szCommon        = "TPM AIK";

        int ret;
        X509_REQ *x509_req = X509_REQ_new();

        try {
            ret = X509_REQ_set_version(x509_req, 1);
            check_openssl(ret, __LINE__);

            // set subject of x509 req
            X509_NAME *x509_name = X509_REQ_get_subject_name(x509_req);
            ret = X509_NAME_add_entry_by_txt(x509_name, "C", MBSTRING_ASC, (const unsigned char*)szCountry, -1, -1, 0);
            check_openssl(ret, __LINE__);
            ret = X509_NAME_add_entry_by_txt(x509_name, "ST", MBSTRING_ASC, (const unsigned char*)szProvince, -1, -1, 0);
            check_openssl(ret, __LINE__);
            ret = X509_NAME_add_entry_by_txt(x509_name, "L", MBSTRING_ASC, (const unsigned char*)szCity, -1, -1, 0);
            check_openssl(ret, __LINE__);
            ret = X509_NAME_add_entry_by_txt(x509_name, "O", MBSTRING_ASC, (const unsigned char*)szOrganization, -1, -1, 0);
            check_openssl(ret, __LINE__);
            ret = X509_NAME_add_entry_by_txt(x509_name, "OU", MBSTRING_ASC, (const unsigned char*)szOrgUnit, -1, -1, 0);
            check_openssl(ret, __LINE__);
            ret = X509_NAME_add_entry_by_txt(x509_name, "CN", MBSTRING_ASC, (const unsigned char*)szCommon, -1, -1, 0);
            check_openssl(ret, __LINE__);

            // get public key
            auto key = tss_load_key(ctx, uuid);
            RSA *sslkey = RSA_new();
            tss_get_rsa_pub(key, sslkey);

            EVP_PKEY *pKey = EVP_PKEY_new();
            ret = EVP_PKEY_assign_RSA(pKey, sslkey);
            check_openssl(ret, __LINE__);

            // set public key
            ret = X509_REQ_set_pubkey(x509_req, pKey);
            check_openssl(ret, __LINE__);

            // x509 req hash
            UINT32 hlen;
            BYTE hdata[20];
            const EVP_MD *md = EVP_sha1();
            ret = X509_REQ_digest(x509_req, md, hdata, &hlen);
            check_openssl(ret, __LINE__);

            printf("hash: ");
            print_bytes(hdata, hlen);
            printf("\n");

            // set hash
            r = Tspi_Hash_SetHashValue(hash, NONCE_LEN, hdata);
            check_tss(r, __LINE__);

            // sign
            UINT32 slen;
            BYTE *sdata;
            r = Tspi_Hash_Sign(hash, key, &slen, &sdata);
            check_tss(r, __LINE__);

            X509_ALGOR *algor = X509_ALGOR_new();
            ASN1_OBJECT *nidobj = OBJ_nid2obj(NID_sha1WithRSAEncryption);
            algor->algorithm = nidobj;

            BIO *stdo = BIO_new_fp(stdout, 0);
            X509_REQ_print(stdo, x509_req);
            printf("\n");

//            ret = X509_REQ_sign(x509_req, pKey, EVP_sha1());
//            check_openssl(ret, __LINE__);

            EVP_PKEY_free(pKey);

            // set hash
//            r = Tspi_Hash_SetHashValue(hash, NONCE_LEN, hdata);
//            check_tss(r, __LINE__);

//            UINT32 slen;
//            BYTE *sdata;
//            r = Tspi_Hash_Sign(hash, key, &slen, &sdata);

//            printf("signature: ");
//            print_bytes(sdata, slen);
//            printf("\n");

//            BIO *out = BIO_new_file("x509_req.pem", "w");
//            ret = PEM_write_bio_X509_REQ(out, x509_req);
//            check_openssl(ret, __LINE__);

        } catch(int l){

            X509_REQ_free(x509_req);
            throw l;
        }

        X509_REQ_free(x509_req);

    } catch(int l){
        eline = l;
    }

    tss_close_context(ctx);
    return eline;
}
