#!/bin/bash

fdat=$1
fsig=$2
fkey=$3

openssl rsautl -verify -in $fsig -inkey $fkey -pubin -asn1parse
sha1sum $fdat

