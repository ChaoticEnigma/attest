
#include "tpm.h"

#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/sha.h>

void u16tobe(BYTE *d, UINT16 n){
    d[1] = n & 0xFF;
    d[0] = (n >> 8) & 0xFF;
}
void u32tobe(BYTE *d, UINT32 n){
    d[3] = n & 0xFF;
    d[2] = (n >> 8) & 0xFF;
    d[1] = (n >> 16) & 0xFF;
    d[0] = (n >> 24) & 0xFF;
}

int main(int argc, char **argv){

    if(argc != 3){
        printf("Usage: quote <uuid> <pcr mask>\n");
        return -1;
    }

    TSS_UUID uuid = decode_uuid(argv[1]);
    UINT32 pcr_mask;
    sscanf(argv[2], "%x", &pcr_mask);

    if(pcr_mask == 0){
        printf("no pcrs selected\n");
        return -2;
    }

    printf("Quoting with Key:\n");
    print_uuid(&uuid);
    printf("\nPCR Mask: %08x\n", pcr_mask);
    printf("\n");

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    int eline = 0;
    TSS_RESULT r;
    TSS_HCONTEXT ctx = tss_open_context_local();

    try {
        // get TPM
        TSS_HTPM tpm = tss_get_tpm(ctx);
        // auth TPM
        tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));
        // auth SRK
        tss_auth_srk(ctx, srk_secret, strlen(srk_secret));
        tss_tpm_reset_dalock(tpm);

        BYTE pcrs[PCR_COUNT][PCR_LEN];

        printf("Platform Configuration Registers:\n");
        for(UINT32 i = 0; i < PCR_COUNT; ++i){
            // read PCR
            UINT32 len;
            BYTE *data;
            r = Tspi_TPM_PcrRead(tpm, i, &len, &data);
            check_tss(r, __LINE__);

            memcpy(pcrs[i], data, PCR_LEN);

            r = Tspi_Context_FreeMemory(ctx, data);
            check_tss(r, __LINE__);

            printf("% 2u: ", i);
            print_bytes(pcrs[i], PCR_LEN);

            if(pcr_mask & (1 << i)){
                printf(" *");
            }

//            printf(" ");
//            r = Tspi_TPM_DirRead(tpm, i, &len, &data);
//            check_tss(r, __LINE__);
//            print_bytes(data, len);

            printf("\n");
        }
        printf("\n");

        BYTE comp[100];
        u16tobe(comp + 0, 3);
        comp[2] = 1 << 0;
        comp[3] = 0;
        comp[4] = 0;
//        u32tobe(comp + 5, 1);
        u32tobe(comp + 5, PCR_LEN * 1);
        memcpy(comp + 5 + 4, pcrs[0], PCR_LEN);

        printf("pcr composite: ");
        print_bytes(comp, 5 + 4 + PCR_LEN);
        printf("\n");

        BYTE hash[20];
        SHA1((BYTE *)&comp, 5 + 4 + PCR_LEN, hash);
        printf("composite hash: ");
        print_bytes(hash, PCR_LEN);
        printf("\n");

        printf("\n");

        // get random data
        BYTE *nonce;
        r = Tspi_TPM_GetRandom(tpm, NONCE_LEN, &nonce);
        check_tss(r, __LINE__);

        printf("nonce: ");
        print_bytes(nonce, NONCE_LEN);
        printf("\n");

        // get quote
        BYTE data[64];
        UINT32 datalen;
        BYTE sig[1024];
        UINT32 siglen;
        tss_quote(ctx, tpm, uuid, pcr_mask, nonce, data, &datalen, sig, &siglen);

        printf("data: ");
        print_bytes(data, datalen);
        printf("\nsignature: ");
        print_bytes(sig, siglen);
        printf("\n");

        // write data
        FILE *fdat = fopen("quote_dat", "wb");
        if(fdat != NULL){
            fwrite(data, datalen, 1, fdat);
            fclose(fdat);
        } else {
            printf("failed to write quote data\n");
        }
        FILE *fsig = fopen("quote_sig", "wb");
        if(fsig != NULL){
            fwrite(sig, siglen, 1, fsig);
            fclose(fsig);
        } else {
            printf("failed to write quote signature\n");
        }

        // extract composite hash
        BYTE chash[PCR_LEN];
        memcpy(chash, data + 8, PCR_LEN);
        printf("pcr composite hash: ");
        print_bytes(chash, PCR_LEN);
        printf("\n");

    } catch(int l){
        eline = l;
    }

    tss_close_context(ctx);
    return eline;
}
