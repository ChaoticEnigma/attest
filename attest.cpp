
#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cstring>
#include <unistd.h>
#include <signal.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

#include <openssl/sha.h>

#define PORT 17771

void u16tobe(BYTE *d, UINT16 n){
    d[1] = n & 0xFF;
    d[0] = (n >> 8) & 0xFF;
}
void u32tobe(BYTE *d, UINT32 n){
    d[3] = n & 0xFF;
    d[2] = (n >> 8) & 0xFF;
    d[1] = (n >> 16) & 0xFF;
    d[0] = (n >> 24) & 0xFF;
}

void format_quote(BYTE *data, UINT32 datalen, BYTE *sig, UINT32 siglen, char *quot){
    char dats[1024];
    sprint_bytes(dats, data, datalen);
    char sigs[1024];
    sprint_bytes(sigs, sig, siglen);

    printf("data: %s\n", dats);
    printf("signature: %s\n", sigs);

    // wite data and signature to quote
    strcat(quot, dats);
    strcat(quot, ":");
    strcat(quot, sigs);
}

static bool run = true;
void sig_handler(int sig){
    printf("Interrupted\n");
    run = false;
}

int main(int argc, char **argv){

    if(argc != 3){
        printf("Usage: attest <uuid> <pcr mask>\n");
        return -1;
    }

    TSS_UUID uuid = decode_uuid(argv[1]);
    UINT32 pcr_mask;
    sscanf(argv[2], "%x", &pcr_mask);

    if(pcr_mask == 0){
        printf("no pcrs selected\n");
        return -2;
    }

    printf("Attesting with Key:\n");
    print_uuid(&uuid);
    printf("\nPCR Mask: %08x\n", pcr_mask);
    printf("\n");

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    struct sigaction act;
    act.sa_handler = sig_handler;
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        printf("Failed to create socket\n");
        return -1;
    }

    int opt = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(PORT);

    int res = bind(sock, (struct sockaddr *)&saddr, sizeof(saddr));
    if(res != 0){
        printf("Failed to bind\n");
        close(sock);
        return -2;
    }

    res = listen(sock, 5);
    if(res != 0){
        printf("Failed to listen\n");
        close(sock);
        return -3;
    }

    printf("Listening...\n");

    // accept connections
    while(run){
        socklen_t len;
        struct sockaddr_in peer;
        int conn = accept(sock, (struct sockaddr *)&peer, &len);
        if(conn < 0){
//            printf("Failed to accept\n");
            continue;
        }
        printf("Connected\n");

        try {
            // read nonce from client
            char buff[128];
            ssize_t s = read(conn, buff, sizeof(buff));
            if(s <= 0)
                throw __LINE__;
            buff[s] = 0;

            if(s < 41)
                throw __LINE__;

            BYTE nonce[NONCE_LEN];
            for(unsigned i = 0; i < NONCE_LEN; ++i){
                int n = sscanf(buff + (i*2), "%02hhx", nonce + i);
                if(n != 1)
                    throw __LINE__;
            }

            printf("nonce: ");
            print_bytes(nonce, NONCE_LEN);
            printf("\n");

            TSS_HCONTEXT ctx = tss_open_context_local();

            // get TPM
            TSS_HTPM tpm = tss_get_tpm(ctx);
            // auth TPM
            tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));
            // auth SRK
            tss_auth_srk(ctx, srk_secret, strlen(srk_secret));
            tss_tpm_reset_dalock(tpm);

            // generate TPM quote
            BYTE data[64];
            UINT32 datalen;
            BYTE sig[1024];
            UINT32 siglen;
            tss_quote(ctx, tpm, uuid, pcr_mask, nonce, data, &datalen, sig, &siglen);

            tss_close_context(ctx);

            char quot[1024] = {0};
            format_quote(data, datalen, sig, siglen, quot);

            size_t ql = strlen(quot);
            printf("quote: %u bytes\n", ql);

            // send quote to client
            s = write(conn, quot, ql);
            if(s != ql)
                throw __LINE__;

        } catch(int l){
            printf("exception: %d\n", l);
        }

        close(conn);
        printf("Disconnected\n");
    }

    printf("Exiting\n");
    close(sock);

    return 0;
}
