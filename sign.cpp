
#include "tpm.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <trousers/tss.h>
#include <trousers/trousers.h>

int main(int argc, char **argv){

    if(argc != 3){
        printf("Usage: sign <uuid> <sha1 hash>\n");
        return -1;
    }

    TSS_UUID uuid = decode_uuid(argv[1]);
    const char *infile = argv[2];

    if(strlen(argv[2]) != 40)
        return -2;

    BYTE hdata[NONCE_LEN];
    for(unsigned i = 0; i < NONCE_LEN; ++i){
        int n = sscanf(argv[2] + (i*2), "%02hhx", hdata + i);
        if(n != 1)
            throw __LINE__;
    }

    printf("Signing with Key:\n");
    print_uuid(&uuid);
    printf("\n");

    printf("hash: ");
    print_bytes(hdata, NONCE_LEN);
    printf("\n");

    char *owner_secret = getenv("TPM_PASS");
    char *srk_secret = getenv("SRK_PASS");

    int eline = 0;
    TSS_RESULT r;
    TSS_HCONTEXT ctx = tss_open_context_local();

    try {
        // get TPM
        TSS_HTPM tpm = tss_get_tpm(ctx);
        // auth TPM
        tss_auth_tpm(tpm, owner_secret, strlen(owner_secret));
        // auth SRK
        tss_auth_srk(ctx, srk_secret, strlen(srk_secret));
        tss_tpm_reset_dalock(tpm);

        // load key
        TSS_HKEY key;
        r = Tspi_Context_LoadKeyByUUID(ctx, TSS_PS_TYPE_SYSTEM, uuid, &key);
        check_tss(r, __LINE__);

        // create hash
        TSS_HHASH hash;
        r = Tspi_Context_CreateObject(ctx, TSS_OBJECT_TYPE_HASH, TSS_HASH_SHA1, &hash);
        check_tss(r, __LINE__);

        // set hash
        r = Tspi_Hash_SetHashValue(hash, NONCE_LEN, hdata);
        check_tss(r, __LINE__);

//        FILE *in = fopen(infile, "rb");
//        if(in == NULL)
//            throw __LINE__;

//        // feed hash
//        size_t s;
//        BYTE buffer[1024];
//        while((s = fread(buffer, 1, 1024, in)) == 1024){
//            r = Tspi_Hash_UpdateHashValue(hash, s, buffer);
//            check_tss(r, __LINE__);
//        }
//        if(ferror(in)){
//            printf("fread error\n");
//            throw __LINE__;
//        }
//        fclose(in);

        // sign
        UINT32 slen;
        BYTE *sdata;
        r = Tspi_Hash_Sign(hash, key, &slen, &sdata);
        check_tss(r, __LINE__);

        printf("signature: ");
        print_bytes(sdata, slen);
        printf("\n");

    } catch(int l){
        eline = l;
    }

    tss_close_context(ctx);
    return eline;
}
