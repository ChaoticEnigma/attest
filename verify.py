
import os, sys
import binascii
import rsa

fpubkey = sys.argv[1]
fmsg = sys.argv[2]
fsig = sys.argv[3]

#msg_hex = open(fmsg, 'r').read().replace(' ', '').replace('\n', '').strip()
#msg_bin = binascii.unhexlify(msg_hex)
msg_bin = open(fmsg, 'rb').read()

#sig_hex = open(fsig, 'r').read().replace(' ', '').replace('\n', '').strip()
#sig_bin = binascii.unhexlify(sig_hex)
sig_bin = open(fsig, 'rb').read()

key = rsa.PublicKey.load_pkcs1_openssl_pem(
    open(fpubkey, 'rb').read()
)

try:
    good_sig = rsa.verify(msg_bin, sig_bin, key)
    print("Signature OK")
except rsa.pkcs1.VerificationError:
    good_sig = False
    print("Signature failed")
