#ifndef TPM_H
#define TPM_H

#include <trousers/tss.h>
#include <openssl/rsa.h>

#define PCR_LEN     20
#define NONCE_LEN   20
#define PCR_COUNT   24

void print_bytes(BYTE *data, UINT32 len);
void sprint_bytes(char *str, BYTE *data, UINT32 len);

TSS_UUID decode_uuid(const char *str);
void encode_uuid(char *str, TSS_UUID uuid);

void print_uuid(TSS_UUID *uuid);
void print_error(TSS_RESULT r);

void check_tss(TSS_RESULT r, int line);

TSS_HCONTEXT tss_open_context_local();
void tss_close_context(TSS_HCONTEXT ctx);

TSS_HTPM tss_get_tpm(TSS_HCONTEXT ctx);
void tss_auth_tpm(TSS_HTPM tpm, char *owner_key, UINT32 keylen);
void tss_tpm_reset_dalock(TSS_HTPM tpm);

void tss_auth_srk(TSS_HCONTEXT ctx, char *srk_key, UINT32 keylen);

TSS_HKEY tss_load_key(TSS_HCONTEXT ctx, TSS_UUID uuid);
void tss_get_rsa_pub(TSS_HKEY key, RSA *sslkey);

int tss_quote(TSS_HCONTEXT ctx, TSS_HTPM tpm, TSS_UUID uuid, UINT32 pcr_mask, BYTE *nonce,
              BYTE *data, UINT32 *datalen, BYTE *sig, UINT32 *siglen);

#endif // TPM_H
